module.exports = {
  base: '/vuepress-pages/',
  title: 'Hello GitLab Pages',
  description: 'Vue Press running on GitLab pages',
  themeConfig: {
    nav: [
      { text: 'Home', link: '/' },
      { text: 'GitLab', link: 'https://gitlab.com' },
    ],
    search: false,
  },
};
